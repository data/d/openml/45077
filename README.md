# OpenML dataset: qsar

https://www.openml.org/d/45077

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The QSAR biodegradation dataset was built in the Milano Chemometrics and QSAR Research Group. The research leading to these results has received funding from the European Communitys Seventh Framework Programme [FP7/2007-2013] under Grant Agreement n. 238701 of Marie Curie ITN Environmental Chemoinformatics (ECO) project.The data have been used to develop QSAR (Quantitative Structure Activity Relationships) models for the study of the relationships between chemical structure and biodegradation of molecules. Biodegradation experimental values of 1055 chemicals were collected from the webpage of the National Institute of Technology and Evaluation of Japan (NITE). Classification models were developed in order to discriminate ready (356) and not ready (699) biodegradable molecules by means of three different modelling methods: k Nearest Neighbours, Partial Least Squares Discriminant Analysis and Support Vector Machines. Details on attributes (molecular descriptors) selected in each model can be found in the quoted reference: Mansouri, K., Ringsted, T., Ballabio, D., Todeschini, R., Consonni, V. (2013). Quantitative Structure - Activity Relationship models for ready biodegradability of chemicals. Journal of Chemical Information and Modeling, 53, 867-878.Source: https://archive.ics.uci.edu/ml/datasets/QSAR+biodegradation

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45077) of an [OpenML dataset](https://www.openml.org/d/45077). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45077/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45077/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45077/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

